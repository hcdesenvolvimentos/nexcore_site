-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 29-Jan-2018 às 11:39
-- Versão do servidor: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_nexcore_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_aiowps_events`
--

DROP TABLE IF EXISTS `nx_aiowps_events`;
CREATE TABLE IF NOT EXISTS `nx_aiowps_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_aiowps_failed_logins`
--

DROP TABLE IF EXISTS `nx_aiowps_failed_logins`;
CREATE TABLE IF NOT EXISTS `nx_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_aiowps_failed_logins`
--

INSERT INTO `nx_aiowps_failed_logins` (`id`, `user_id`, `user_login`, `failed_login_date`, `login_attempt_ip`) VALUES
(1, 1, 'nexcore', '2018-01-18 21:05:14', '::1'),
(2, 0, 'sumera', '2018-01-29 09:30:41', '::1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_aiowps_global_meta`
--

DROP TABLE IF EXISTS `nx_aiowps_global_meta`;
CREATE TABLE IF NOT EXISTS `nx_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_aiowps_login_activity`
--

DROP TABLE IF EXISTS `nx_aiowps_login_activity`;
CREATE TABLE IF NOT EXISTS `nx_aiowps_login_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_aiowps_login_activity`
--

INSERT INTO `nx_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'nexcore', '2018-01-18 21:05:18', '0000-00-00 00:00:00', '::1', '', ''),
(2, 1, 'nexcore', '2018-01-20 22:23:35', '0000-00-00 00:00:00', '::1', '', ''),
(3, 1, 'nexcore', '2018-01-21 22:12:11', '0000-00-00 00:00:00', '::1', '', ''),
(4, 1, 'nexcore', '2018-01-29 09:30:53', '0000-00-00 00:00:00', '::1', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_aiowps_login_lockdown`
--

DROP TABLE IF EXISTS `nx_aiowps_login_lockdown`;
CREATE TABLE IF NOT EXISTS `nx_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_aiowps_permanent_block`
--

DROP TABLE IF EXISTS `nx_aiowps_permanent_block`;
CREATE TABLE IF NOT EXISTS `nx_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_commentmeta`
--

DROP TABLE IF EXISTS `nx_commentmeta`;
CREATE TABLE IF NOT EXISTS `nx_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_comments`
--

DROP TABLE IF EXISTS `nx_comments`;
CREATE TABLE IF NOT EXISTS `nx_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_comments`
--

INSERT INTO `nx_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-01-16 20:51:34', '2018-01-16 22:51:34', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_links`
--

DROP TABLE IF EXISTS `nx_links`;
CREATE TABLE IF NOT EXISTS `nx_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_options`
--

DROP TABLE IF EXISTS `nx_options`;
CREATE TABLE IF NOT EXISTS `nx_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=390 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_options`
--

INSERT INTO `nx_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/nexcore_site/', 'yes'),
(2, 'home', 'http://localhost/projetos/nexcore_site/', 'yes'),
(3, 'blogname', 'Blog Nexcore', 'yes'),
(4, 'blogdescription', 'Blog Nexcore', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'contato@hudsoncarolino.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:112:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(295, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(296, 'wpseo_sitemap_30_cache_validator', 'dyyO', 'no'),
(297, 'wpseo_taxonomy_meta', 'a:1:{s:8:\"category\";a:1:{i:1;a:2:{s:13:\"wpseo_linkdex\";s:2:\"-9\";s:19:\"wpseo_content_score\";s:2:\"30\";}}}', 'yes'),
(301, 'wpseo_sitemap_45_cache_validator', 'lkkv', 'no'),
(303, 'wpseo_sitemap_47_cache_validator', 'lrDS', 'no'),
(307, 'wpseo_sitemap_53_cache_validator', 'lX5m', 'no'),
(309, 'wpseo_sitemap_55_cache_validator', 'm4Uh', 'no'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:29:\"base-nexcore/base-nexcore.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:32:\"disqus-comment-system/disqus.php\";i:5;s:21:\"meta-box/meta-box.php\";i:6;s:24:\"wordpress-seo/wp-seo.php\";i:7;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'nexcore', 'yes'),
(41, 'stylesheet', 'nexcore', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'nx_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:68:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'pt_BR', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'nonce_key', 'j8%x*yIPE+fS+XLJ[4dx)}Yl)p=Z<*<F^4Q&,XUv`kz2C^4hh@7H~8y)lSfm-6M+', 'no'),
(108, 'nonce_salt', 'MpZPoKR3~H gfWjN6I6m5&g-`O3us[lB8VShHl7Q/HPgYR0P.w:X82gQ%u5MxZo[', 'no'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:8:{i:1517226059;a:1:{s:25:\"wpseo_ping_search_engines\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1517226966;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1517266295;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1517266316;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1517266566;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1517266568;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1517267204;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1516144537;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(124, 'auth_key', '+pZtY?I+FI5$;3C.a}dC>$o-Qr.Xd$!Uaqk^9y 8D2:u4LwhFSd&N>w >u%I|oP{', 'no'),
(217, 'configuracao', 'a:9:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:5:{s:3:\"url\";s:74:\"http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/logo.png\";s:2:\"id\";s:2:\"27\";s:6:\"height\";s:3:\"299\";s:5:\"width\";s:4:\"1196\";s:9:\"thumbnail\";s:82:\"http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/logo-150x150.png\";}s:15:\"opt_logo_branca\";a:5:{s:3:\"url\";s:80:\"http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/logoRodape.png\";s:2:\"id\";s:2:\"28\";s:6:\"height\";s:2:\"47\";s:5:\"width\";s:3:\"195\";s:9:\"thumbnail\";s:87:\"http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/logoRodape-150x47.png\";}s:14:\"opt_telefoneSP\";s:27:\"SÃO PAULO - (11) 3010-3288\";s:15:\"opt_telefoneCWB\";s:25:\"CURITIBA - (41) 3322-1336\";s:9:\"opt_email\";s:0:\"\";s:18:\"redes_sociais_face\";s:1:\"#\";s:18:\"redes_sociais_link\";s:1:\"#\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(374, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.2\";s:7:\"version\";s:5:\"4.9.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1517225458;s:15:\"version_checked\";s:5:\"4.9.2\";s:12:\"translations\";a:0:{}}', 'no'),
(209, 'current_theme', 'Nexcore', 'yes'),
(210, 'theme_mods_twentyfifteen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1516144541;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(211, 'theme_switched', '', 'yes'),
(125, 'auth_salt', '&E.UGVT9?vf;8L,IG&c-j.S(K7M+2Y!oQ^hwS2CHPwTk/Eda@ii#Vs#7gIb1Yvk-', 'no'),
(370, '_site_transient_timeout_theme_roots', '1517227254', 'no'),
(371, '_site_transient_theme_roots', 'a:2:{s:7:\"nexcore\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";}', 'no'),
(377, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1517225461;s:7:\"checked\";a:2:{s:7:\"nexcore\";s:5:\"1.0.0\";s:13:\"twentyfifteen\";s:3:\"1.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(378, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1517225463;s:7:\"checked\";a:8:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.3.1\";s:29:\"base-nexcore/base-nexcore.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"4.9.2\";s:32:\"disqus-comment-system/disqus.php\";s:4:\"2.87\";s:28:\"wysija-newsletters/index.php\";s:5:\"2.8.1\";s:21:\"meta-box/meta-box.php\";s:6:\"4.13.1\";s:35:\"redux-framework/redux-framework.php\";s:7:\"3.6.7.7\";s:24:\"wordpress-seo/wp-seo.php\";s:3:\"6.2\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-06-04 13:54:56\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/4.9.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"6.2\";s:7:\"updated\";s:19:\"2017-12-20 08:41:34\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/6.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:7:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.3.1\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";s:7:\"default\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1232826\";s:7:\"default\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1232826\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"4.9.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.4.9.2.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:7:\"default\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";s:7:\"default\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:4:\"2.87\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/disqus-comment-system.2.87.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:7:\"default\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";s:7:\"default\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:5:\"2.8.1\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.8.1.zip\";s:5:\"icons\";a:4:{s:2:\"1x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-128x128.png?rev=1703780\";s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:7:\"default\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";s:7:\"default\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.13.1\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.13.1.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";s:7:\"default\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382\";s:7:\"default\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:7:\"3.6.7.7\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.7.7.zip\";s:5:\"icons\";a:4:{s:2:\"1x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-128x128.png?rev=995554\";s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:7:\"default\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";s:7:\"default\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:3:\"6.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/wordpress-seo.6.2.zip\";s:5:\"icons\";a:4:{s:2:\"1x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-128x128.png?rev=1550389\";s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1550389\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1203032\";s:7:\"default\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1203032\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1695112\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1695112\";s:7:\"default\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1695112\";}s:11:\"banners_rtl\";a:3:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1695112\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1695112\";s:7:\"default\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1695112\";}}}}', 'no'),
(372, '_site_transient_timeout_browser_b876c8fd7fc402e60530b64622320f7a', '1517830255', 'no'),
(126, 'logged_in_key', 'n[w!cm4m,#-AE194Y%$N`yz8%)Uk^6IDr5,DvW|2yFwT(i1J:SP;j?{nO[|V=//-', 'no'),
(127, 'logged_in_salt', 'd(w9tso~B2IyLu a+|?5k%e,wrOQ0guMGWe3:oQm7ydpPv89XQkddxx;h#BDFF4%', 'no'),
(139, 'recently_activated', 'a:0:{}', 'yes'),
(140, 'can_compress_scripts', '1', 'no'),
(163, 'wpseo', 'a:25:{s:14:\"blocking_files\";a:0:{}s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:3:\"6.2\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:12:\"website_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:20:\"enable_setting_pages\";b:0;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1516143367;}', 'yes'),
(164, 'wpseo_permalinks', 'a:9:{s:15:\"cleanpermalinks\";b:0;s:24:\"cleanpermalink-extravars\";s:0:\"\";s:29:\"cleanpermalink-googlecampaign\";b:0;s:31:\"cleanpermalink-googlesitesearch\";b:0;s:15:\"cleanreplytocom\";b:0;s:10:\"cleanslugs\";b:1;s:18:\"redirectattachment\";b:0;s:17:\"stripcategorybase\";b:0;s:13:\"trailingslash\";b:0;}', 'yes'),
(165, 'wpseo_titles', 'a:53:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:15:\"usemetakeywords\";b:0;s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:18:\"metakey-home-wpseo\";s:0:\"\";s:20:\"metakey-author-wpseo\";s:0:\"\";s:22:\"noindex-subpages-wpseo\";b:0;s:20:\"noindex-author-wpseo\";b:0;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"metakey-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:16:\"hideeditbox-post\";b:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"metakey-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:16:\"hideeditbox-page\";b:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"metakey-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:22:\"hideeditbox-attachment\";b:0;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:20:\"metakey-tax-category\";s:0:\"\";s:24:\"hideeditbox-tax-category\";b:0;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:20:\"metakey-tax-post_tag\";s:0:\"\";s:24:\"hideeditbox-tax-post_tag\";b:0;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:23:\"metakey-tax-post_format\";s:0:\"\";s:27:\"hideeditbox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;}', 'yes'),
(166, 'wpseo_social', 'a:20:{s:9:\"fb_admins\";a:0:{}s:12:\"fbconnectkey\";s:32:\"c97c3941e28f4d472ff1cdf747e7376a\";s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(167, 'wpseo_rss', 'a:2:{s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";}', 'yes'),
(168, 'wpseo_internallinks', 'a:10:{s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:23:\"breadcrumbs-blog-remove\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:23:\"post_types-post-maintax\";i:0;}', 'yes'),
(169, 'wpseo_xml', 'a:16:{s:22:\"disable_author_sitemap\";b:1;s:22:\"disable_author_noposts\";b:1;s:16:\"enablexmlsitemap\";b:1;s:16:\"entries-per-page\";i:1000;s:14:\"excluded-posts\";s:0:\"\";s:38:\"user_role-administrator-not_in_sitemap\";b:0;s:31:\"user_role-editor-not_in_sitemap\";b:0;s:31:\"user_role-author-not_in_sitemap\";b:0;s:36:\"user_role-contributor-not_in_sitemap\";b:0;s:35:\"user_role-subscriber-not_in_sitemap\";b:0;s:30:\"post_types-post-not_in_sitemap\";b:0;s:30:\"post_types-page-not_in_sitemap\";b:0;s:36:\"post_types-attachment-not_in_sitemap\";b:1;s:34:\"taxonomies-category-not_in_sitemap\";b:0;s:34:\"taxonomies-post_tag-not_in_sitemap\";b:0;s:37:\"taxonomies-post_format-not_in_sitemap\";b:0;}', 'yes'),
(170, 'wpseo_flush_rewrite', '1', 'yes'),
(171, '_transient_timeout_wpseo_link_table_inaccessible', '1547679368', 'no'),
(172, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(173, '_transient_timeout_wpseo_meta_table_inaccessible', '1547679368', 'no'),
(174, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(159, 'aiowpsec_db_version', '1.9', 'yes'),
(160, 'aio_wp_security_configs', 'a:83:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:0:\"\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:0:\"\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";s:1:\"3\";s:24:\"aiowps_retry_time_period\";s:1:\"5\";s:26:\"aiowps_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:29:\"contato@hudsoncarolino.com.br\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"qjoao64dmvq9qiglsere\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_custom_login_captcha\";s:0:\"\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"zfmc1wjg6ofd34t501me\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:0:\"\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:29:\"contato@hudsoncarolino.com.br\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:0:\"\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:0:\"\";s:26:\"aiowps_disable_index_views\";s:0:\"\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:0:\"\";s:34:\"aiowps_advanced_char_string_filter\";s:0:\"\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:0:\"\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:0:\"\";s:29:\"aiowps_enable_comment_captcha\";s:0:\"\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:29:\"contato@hudsoncarolino.com.br\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";}', 'yes'),
(161, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"4.9.2\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1516136166;s:7:\"version\";s:5:\"4.9.2\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(185, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1516924863;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1516229791;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1517312094;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1517357208;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1518562591;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}}', 'yes'),
(177, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(181, 'installation_step', '16', 'yes'),
(179, 'wysija_post_type_updated', '1516143370', 'yes'),
(180, 'wysija_post_type_created', '1516143370', 'yes'),
(182, 'wysija', 'YToxNzp7czo5OiJmcm9tX25hbWUiO3M6NzoibmV4Y29yZSI7czoxMjoicmVwbHl0b19uYW1lIjtzOjc6Im5leGNvcmUiO3M6MTU6ImVtYWlsc19ub3RpZmllZCI7czoyOToiY29udGF0b0BodWRzb25jYXJvbGluby5jb20uYnIiO3M6MTA6ImZyb21fZW1haWwiO3M6MTQ6ImluZm9AbG9jYWxob3N0IjtzOjEzOiJyZXBseXRvX2VtYWlsIjtzOjE0OiJpbmZvQGxvY2FsaG9zdCI7czoxNToiZGVmYXVsdF9saXN0X2lkIjtpOjE7czoxNzoidG90YWxfc3Vic2NyaWJlcnMiO3M6MToiMSI7czoxNjoiaW1wb3J0d3BfbGlzdF9pZCI7aToyO3M6MTg6ImNvbmZpcm1fZW1haWxfbGluayI7aTo1O3M6MTI6InVwbG9hZGZvbGRlciI7czo2MDoiQzpcd2FtcFx3d3dccHJvamV0b3NcbmV4Y29yZV9zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkc1x3eXNpamFcIjtzOjk6InVwbG9hZHVybCI7czo2NToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9uZXhjb3JlX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS8iO3M6MTY6ImNvbmZpcm1fZW1haWxfaWQiO2k6MjtzOjk6Imluc3RhbGxlZCI7YjoxO3M6MjA6Im1hbmFnZV9zdWJzY3JpcHRpb25zIjtiOjE7czoxNDoiaW5zdGFsbGVkX3RpbWUiO2k6MTUxNjE0MzM3MztzOjE3OiJ3eXNpamFfZGJfdmVyc2lvbiI7czo1OiIyLjguMSI7czoxMToiZGtpbV9kb21haW4iO3M6OToibG9jYWxob3N0Ijt9', 'yes'),
(183, 'wysija_reinstall', '0', 'no'),
(208, 'wpseo_sitemap_destaque_cache_validator', '74MUX', 'no'),
(188, 'wpseo_sitemap_1_cache_validator', '41ZwK', 'no'),
(189, 'wpseo_sitemap_category_cache_validator', '41ZwN', 'no'),
(190, 'wpseo_sitemap_post_cache_validator', '41ZwQ', 'no'),
(381, 'category_children', 'a:0:{}', 'yes'),
(379, '_transient_timeout_plugin_slugs', '1517311864', 'no');
INSERT INTO `nx_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(380, '_transient_plugin_slugs', 'a:8:{i:0;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:1;s:29:\"base-nexcore/base-nexcore.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:32:\"disqus-comment-system/disqus.php\";i:4;s:28:\"wysija-newsletters/index.php\";i:5;s:21:\"meta-box/meta-box.php\";i:6;s:35:\"redux-framework/redux-framework.php\";i:7;s:24:\"wordpress-seo/wp-seo.php\";}', 'no'),
(286, 'new_admin_email', 'contato@hudsoncarolino.com.br', 'yes'),
(215, '_transient_timeout__redux_activation_redirect', '1517225878', 'no'),
(216, '_transient__redux_activation_redirect', '1', 'no'),
(214, 'redux_version_upgraded_from', '3.6.7.7', 'yes'),
(196, 'wpseo_sitemap_7_cache_validator', '72HxX', 'no'),
(197, 'wpseo_sitemap_post_tag_cache_validator', '76efD', 'no'),
(218, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1517225575;}', 'yes'),
(213, 'theme_mods_nexcore', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(255, 'wpseo_sitemap_servico_cache_validator', '6PeOA', 'no'),
(225, 'wysija_last_php_cron_call', '1517225694', 'yes'),
(229, 'wysija_check_pn', '1517225694.6663', 'yes'),
(230, 'wysija_last_scheduled_check', '1517225694', 'yes'),
(366, 'wpseo_sitemap_cache_validator_global', '3Zmyy', 'no'),
(368, '_transient_timeout_users_online', '1517227254', 'no'),
(369, '_transient_users_online', 'a:1:{i:0;a:3:{s:7:\"user_id\";i:1;s:13:\"last_activity\";d:1517218254;s:10:\"ip_address\";s:3:\"::1\";}}', 'no'),
(373, '_site_transient_browser_b876c8fd7fc402e60530b64622320f7a', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"63.0.3239.132\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(382, '_site_transient_timeout_available_translations', '1517236297', 'no'),
(383, '_site_transient_available_translations', 'a:111:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-01 13:40:41\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-27 09:27:02\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-14 16:27:27\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.2/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 09:53:15\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.8.5\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.5/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-05 09:44:12\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-04 20:20:28\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-29 05:52:09\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-10 17:55:47\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-22 16:19:20\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-14 18:20:23\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-22 15:38:30\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-22 15:43:53\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/4.9.2/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-28 20:27:03\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-28 20:27:48\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.9.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-06 10:31:42\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-23 18:53:44\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 09:12:07\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 09:10:37\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 09:54:30\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-16 08:56:09\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-14 01:23:37\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 23:17:08\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 15:03:42\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-16 03:15:17\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-18 11:09:35\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"4.8.5\";s:7:\"updated\";s:19:\"2017-07-30 16:09:17\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.5/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"4.8.5\";s:7:\"updated\";s:19:\"2017-07-31 15:12:02\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.5/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-10-01 17:54:52\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.3/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-28 20:09:49\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-19 14:11:29\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 21:12:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-14 15:13:01\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 09:48:14\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-19 23:55:33\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-09 09:23:29\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-14 10:19:51\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-16 10:40:05\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-16 11:06:53\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-06 13:23:01\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-02 23:26:33\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-14 13:03:07\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-14 10:14:07\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-21 02:45:34\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-04-13 13:55:54\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-08 14:46:48\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-17 09:56:44\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-14 11:47:57\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-07 12:32:16\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-09 14:06:54\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.2/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-07 02:07:59\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-04 01:44:20\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:25\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 19:40:23\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-03-17 20:40:40\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.7/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:5:\"4.8.5\";s:7:\"updated\";s:19:\"2017-09-30 06:25:41\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.5/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 00:51:20\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-17 19:14:57\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-10-05 06:45:20\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.3/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-22 08:05:07\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-06 06:13:30\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-22 08:13:09\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.9.2/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-09-25 10:02:16\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.3/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-11 12:23:44\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-19 23:04:20\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-01 14:17:04\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-18 12:10:14\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/4.9.2/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 18:30:47\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-15 20:59:00\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-08 12:38:03\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-30 17:20:03\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 23:19:48\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-08 22:15:45\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-10-07 02:08:56\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.3/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-20 16:20:13\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-05 09:23:39\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:9:\"Uyƣurqə\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-11-02 17:05:02\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.3/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-20 11:26:53\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-28 12:41:50\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 10:43:28\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-02 09:46:12\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 02:29:44\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-17 22:20:52\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no'),
(384, '_transient_timeout_select2-css_style_cdn_is_up', '1517311933', 'no'),
(385, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(386, '_transient_timeout_select2-js_script_cdn_is_up', '1517311933', 'no'),
(387, '_transient_select2-js_script_cdn_is_up', '1', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_postmeta`
--

DROP TABLE IF EXISTS `nx_postmeta`;
CREATE TABLE IF NOT EXISTS `nx_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=195 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_postmeta`
--

INSERT INTO `nx_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(3, 4, '_mail', 'a:8:{s:7:\"subject\";s:24:\"Nexcore \"[your-subject]\"\";s:6:\"sender\";s:43:\"[your-name] <contato@hudsoncarolino.com.br>\";s:4:\"body\";s:184:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Nexcore (http://localhost/projetos/nexcore_site)\";s:9:\"recipient\";s:29:\"contato@hudsoncarolino.com.br\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(4, 4, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"Nexcore \"[your-subject]\"\";s:6:\"sender\";s:39:\"Nexcore <contato@hudsoncarolino.com.br>\";s:4:\"body\";s:126:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Nexcore (http://localhost/projetos/nexcore_site)\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:39:\"Reply-To: contato@hudsoncarolino.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(5, 4, '_messages', 'a:8:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";}'),
(6, 4, '_additional_settings', NULL),
(7, 4, '_locale', 'pt_BR'),
(8, 1, '_wp_trash_meta_status', 'publish'),
(9, 1, '_wp_trash_meta_time', '1516144000'),
(10, 1, '_wp_desired_post_slug', 'ola-mundo'),
(11, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(12, 7, '_edit_last', '1'),
(13, 7, '_edit_lock', '1516582285:1'),
(19, 11, '_wp_attached_file', '2018/01/blog3.jpg'),
(15, 7, '_yoast_wpseo_content_score', '30'),
(16, 7, 'Nexcore_subtitulo_post', 'Transforme sua central de atendimento em uma máquina de gerar lucros'),
(17, 7, '_yoast_wpseo_primary_category', '2'),
(20, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:673;s:4:\"file\";s:17:\"2018/01/blog3.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"blog3-300x210.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:210;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"blog3-768x538.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:538;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:17:\"blog3-600x421.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:421;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 12, '_wp_attached_file', '2018/01/bannerBlog.jpg'),
(22, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:669;s:6:\"height\";i:431;s:4:\"file\";s:22:\"2018/01/bannerBlog.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"bannerBlog-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"bannerBlog-300x193.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:193;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"bannerBlog-600x387.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:387;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(23, 13, '_wp_attached_file', '2018/01/blog1.png'),
(24, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:389;s:6:\"height\";i:185;s:4:\"file\";s:17:\"2018/01/blog1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"blog1-300x143.png\";s:5:\"width\";i:300;s:6:\"height\";i:143;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 14, '_wp_attached_file', '2018/01/blog3.png'),
(26, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:389;s:6:\"height\";i:186;s:4:\"file\";s:17:\"2018/01/blog3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"blog3-300x143.png\";s:5:\"width\";i:300;s:6:\"height\";i:143;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(27, 15, '_wp_attached_file', '2018/01/blogSingle.png'),
(28, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1343;s:6:\"height\";i:467;s:4:\"file\";s:22:\"2018/01/blogSingle.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"blogSingle-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"blogSingle-300x104.png\";s:5:\"width\";i:300;s:6:\"height\";i:104;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"blogSingle-768x267.png\";s:5:\"width\";i:768;s:6:\"height\";i:267;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"blogSingle-1024x356.png\";s:5:\"width\";i:1024;s:6:\"height\";i:356;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:22:\"blogSingle-600x209.png\";s:5:\"width\";i:600;s:6:\"height\";i:209;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 16, '_wp_attached_file', '2018/01/blog.png'),
(30, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:392;s:6:\"height\";i:182;s:4:\"file\";s:16:\"2018/01/blog.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"blog-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"blog-300x139.png\";s:5:\"width\";i:300;s:6:\"height\";i:139;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 7, '_thumbnail_id', '12'),
(33, 17, '_edit_last', '1'),
(34, 17, '_edit_lock', '1516316601:1'),
(35, 18, '_wp_attached_file', '2018/01/blog.jpg'),
(36, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:400;s:4:\"file\";s:16:\"2018/01/blog.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"blog-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"blog-300x125.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"blog-768x320.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:320;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:16:\"blog-600x250.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(37, 17, '_thumbnail_id', '18'),
(38, 17, '_yoast_wpseo_content_score', '30'),
(39, 23, '_edit_last', '1'),
(40, 23, '_edit_lock', '1516316625:1'),
(41, 23, '_thumbnail_id', '11'),
(42, 23, '_yoast_wpseo_content_score', '30'),
(43, 24, '_edit_last', '1'),
(44, 24, '_edit_lock', '1516317394:1'),
(45, 24, '_thumbnail_id', '16'),
(46, 24, '_yoast_wpseo_content_score', '30'),
(48, 25, '_edit_last', '1'),
(49, 25, '_edit_lock', '1516497027:1'),
(50, 25, '_thumbnail_id', '13'),
(52, 25, '_yoast_wpseo_content_score', '30'),
(53, 25, '_yoast_wpseo_primary_category', ''),
(55, 25, 'Nexcore_subtitulo_post', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período.'),
(56, 27, '_wp_attached_file', '2018/01/logo.png'),
(57, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1196;s:6:\"height\";i:299;s:4:\"file\";s:16:\"2018/01/logo.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x75.png\";s:5:\"width\";i:300;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"logo-768x192.png\";s:5:\"width\";i:768;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"logo-1024x256.png\";s:5:\"width\";i:1024;s:6:\"height\";i:256;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:16:\"logo-600x150.png\";s:5:\"width\";i:600;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(58, 28, '_wp_attached_file', '2018/01/logoRodape.png'),
(59, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:195;s:6:\"height\";i:47;s:4:\"file\";s:22:\"2018/01/logoRodape.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"logoRodape-150x47.png\";s:5:\"width\";i:150;s:6:\"height\";i:47;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(60, 29, '_menu_item_type', 'custom'),
(61, 29, '_menu_item_menu_item_parent', '0'),
(62, 29, '_menu_item_object_id', '29'),
(63, 29, '_menu_item_object', 'custom'),
(64, 29, '_menu_item_target', ''),
(65, 29, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(66, 29, '_menu_item_xfn', ''),
(67, 29, '_menu_item_url', 'http://localhost/htdocs/projetos/nexcore_blog/'),
(79, 31, '_menu_item_menu_item_parent', '0'),
(135, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:720;s:4:\"file\";s:32:\"2018/01/diary-968603_960_720.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"diary-968603_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"diary-968603_960_720-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"diary-968603_960_720-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:32:\"diary-968603_960_720-600x450.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(134, 38, '_wp_attached_file', '2018/01/diary-968603_960_720.jpg'),
(133, 37, '_edit_lock', '1516582284:1'),
(132, 37, '_edit_last', '1'),
(78, 31, '_menu_item_type', 'custom'),
(80, 31, '_menu_item_object_id', '31'),
(81, 31, '_menu_item_object', 'custom'),
(82, 31, '_menu_item_target', ''),
(83, 31, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(84, 31, '_menu_item_xfn', ''),
(85, 31, '_menu_item_url', 'http://Anexcore'),
(87, 32, '_menu_item_type', 'custom'),
(88, 32, '_menu_item_menu_item_parent', '0'),
(89, 32, '_menu_item_object_id', '32'),
(90, 32, '_menu_item_object', 'custom'),
(91, 32, '_menu_item_target', ''),
(92, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(93, 32, '_menu_item_xfn', ''),
(94, 32, '_menu_item_url', 'http://Soluções'),
(96, 33, '_menu_item_type', 'custom'),
(97, 33, '_menu_item_menu_item_parent', '0'),
(98, 33, '_menu_item_object_id', '33'),
(99, 33, '_menu_item_object', 'custom'),
(100, 33, '_menu_item_target', ''),
(101, 33, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(102, 33, '_menu_item_xfn', ''),
(103, 33, '_menu_item_url', 'http://Produtos'),
(105, 34, '_menu_item_type', 'custom'),
(106, 34, '_menu_item_menu_item_parent', '0'),
(107, 34, '_menu_item_object_id', '34'),
(108, 34, '_menu_item_object', 'custom'),
(109, 34, '_menu_item_target', ''),
(110, 34, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(111, 34, '_menu_item_xfn', ''),
(112, 34, '_menu_item_url', 'http://Serviços'),
(114, 35, '_menu_item_type', 'custom'),
(115, 35, '_menu_item_menu_item_parent', '0'),
(116, 35, '_menu_item_object_id', '35'),
(117, 35, '_menu_item_object', 'custom'),
(118, 35, '_menu_item_target', ''),
(119, 35, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(120, 35, '_menu_item_xfn', ''),
(121, 35, '_menu_item_url', 'http://Blog'),
(123, 36, '_menu_item_type', 'custom'),
(124, 36, '_menu_item_menu_item_parent', '0'),
(125, 36, '_menu_item_object_id', '36'),
(126, 36, '_menu_item_object', 'custom'),
(127, 36, '_menu_item_target', ''),
(128, 36, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(129, 36, '_menu_item_xfn', ''),
(130, 36, '_menu_item_url', 'http://Contato'),
(136, 39, '_wp_attached_file', '2018/01/home-office-336378_960_720.jpg'),
(137, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:38:\"2018/01/home-office-336378_960_720.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"home-office-336378_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"home-office-336378_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:38:\"home-office-336378_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:38:\"home-office-336378_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(138, 40, '_wp_attached_file', '2018/01/movement-2203657_960_720.jpg'),
(139, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:639;s:4:\"file\";s:36:\"2018/01/movement-2203657_960_720.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"movement-2203657_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"movement-2203657_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"movement-2203657_960_720-768x511.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:511;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"movement-2203657_960_720-600x399.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:399;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(140, 41, '_wp_attached_file', '2018/01/office-581131_960_720.jpg'),
(141, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:33:\"2018/01/office-581131_960_720.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"office-581131_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"office-581131_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"office-581131_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:33:\"office-581131_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(142, 42, '_wp_attached_file', '2018/01/statistic-1820320_960_720.png'),
(143, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:942;s:6:\"height\";i:720;s:4:\"file\";s:37:\"2018/01/statistic-1820320_960_720.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"statistic-1820320_960_720-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"statistic-1820320_960_720-300x229.png\";s:5:\"width\";i:300;s:6:\"height\";i:229;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"statistic-1820320_960_720-768x587.png\";s:5:\"width\";i:768;s:6:\"height\";i:587;s:9:\"mime-type\";s:9:\"image/png\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:37:\"statistic-1820320_960_720-600x459.png\";s:5:\"width\";i:600;s:6:\"height\";i:459;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(144, 43, '_wp_attached_file', '2018/01/workspace-766045_960_720.jpg'),
(145, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:36:\"2018/01/workspace-766045_960_720.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"workspace-766045_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"workspace-766045_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"workspace-766045_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:36:\"workspace-766045_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(146, 37, '_thumbnail_id', '43'),
(150, 45, '_edit_last', '1'),
(148, 37, '_yoast_wpseo_content_score', '30'),
(149, 37, '_yoast_wpseo_primary_category', '5'),
(151, 45, '_edit_lock', '1516582284:1'),
(152, 45, '_thumbnail_id', '41'),
(156, 47, '_edit_last', '1'),
(154, 45, '_yoast_wpseo_content_score', '30'),
(155, 45, '_yoast_wpseo_primary_category', '5'),
(157, 47, '_edit_lock', '1516582283:1'),
(158, 47, '_thumbnail_id', '39'),
(162, 49, '_edit_last', '1'),
(160, 47, '_yoast_wpseo_content_score', '30'),
(161, 47, '_yoast_wpseo_primary_category', '2'),
(163, 49, '_edit_lock', '1516582283:1'),
(164, 49, '_thumbnail_id', '42'),
(168, 51, '_edit_last', '1'),
(166, 49, '_yoast_wpseo_content_score', '30'),
(167, 49, '_yoast_wpseo_primary_category', '2'),
(169, 51, '_edit_lock', '1517225761:1'),
(170, 51, '_thumbnail_id', '39'),
(174, 53, '_edit_last', '1'),
(172, 51, '_yoast_wpseo_content_score', '30'),
(173, 51, '_yoast_wpseo_primary_category', '1'),
(175, 53, '_edit_lock', '1517225752:1'),
(176, 53, '_thumbnail_id', '40'),
(180, 55, '_edit_last', '1'),
(178, 53, '_yoast_wpseo_content_score', '30'),
(179, 53, '_yoast_wpseo_primary_category', '7'),
(181, 55, '_edit_lock', '1517225764:1'),
(182, 55, '_thumbnail_id', '38'),
(184, 55, '_yoast_wpseo_content_score', '30'),
(185, 55, '_yoast_wpseo_primary_category', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_posts`
--

DROP TABLE IF EXISTS `nx_posts`;
CREATE TABLE IF NOT EXISTS `nx_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_posts`
--

INSERT INTO `nx_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-01-16 20:51:34', '2018-01-16 22:51:34', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Olá, mundo!', '', 'trash', 'open', 'open', '', 'ola-mundo__trashed', '', '', '2018-01-16 21:06:40', '2018-01-16 23:06:40', '', 0, 'http://localhost/projetos/nexcore_site/?p=1', 0, 'post', '', 1),
(2, 1, '2018-01-16 20:51:34', '2018-01-16 22:51:34', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página \'Sobre\' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins-de-semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href=\"http://localhost/projetos/nexcore_site/wp-admin/\">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2018-01-16 20:51:34', '2018-01-16 22:51:34', '', 0, 'http://localhost/projetos/nexcore_site/?page_id=2', 0, 'page', '', 0),
(4, 1, '2018-01-16 20:56:06', '2018-01-16 22:56:06', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]\nNexcore \"[your-subject]\"\n[your-name] <contato@hudsoncarolino.com.br>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Nexcore (http://localhost/projetos/nexcore_site)\ncontato@hudsoncarolino.com.br\nReply-To: [your-email]\n\n0\n0\n\nNexcore \"[your-subject]\"\nNexcore <contato@hudsoncarolino.com.br>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Nexcore (http://localhost/projetos/nexcore_site)\n[your-email]\nReply-To: contato@hudsoncarolino.com.br\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-01-16 20:56:06', '2018-01-16 22:56:06', '', 0, 'http://localhost/projetos/nexcore_site/?post_type=wpcf7_contact_form&p=4', 0, 'wpcf7_contact_form', '', 0),
(5, 1, '2018-01-16 20:56:13', '2018-01-16 22:56:13', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2018-01-16 20:56:13', '2018-01-16 22:56:13', '', 0, 'http://localhost/projetos/nexcore_site/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(6, 1, '2018-01-16 21:06:40', '2018-01-16 23:06:40', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Olá, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-01-16 21:06:40', '2018-01-16 23:06:40', '', 1, 'http://localhost/projetos/nexcore_site/2018/01/16/1-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2018-01-16 21:08:37', '2018-01-16 23:08:37', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', '3 tendências para Call Centers em 2018', '', 'publish', 'open', 'open', '', '3-tendencias-para-call-centers-em-2018', '', '', '2018-01-18 21:24:19', '2018-01-18 23:24:19', '', 0, 'http://localhost/projetos/nexcore_site/?p=7', 0, 'post', '', 0),
(8, 1, '2018-01-16 21:08:37', '2018-01-16 23:08:37', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', '3 tendências para Call Centers em 2018', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-01-16 21:08:37', '2018-01-16 23:08:37', '', 7, 'http://localhost/projetos/nexcore_site/2018/01/16/7-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-01-16 21:16:24', '2018-01-16 23:16:24', '', 'blog3', '', 'inherit', 'open', 'closed', '', 'blog3', '', '', '2018-01-16 21:16:24', '2018-01-16 23:16:24', '', 7, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/blog3.jpg', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2018-01-16 21:16:37', '2018-01-16 23:16:37', '', 'bannerBlog', '', 'inherit', 'open', 'closed', '', 'bannerblog', '', '', '2018-01-16 21:16:37', '2018-01-16 23:16:37', '', 7, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/bannerBlog.jpg', 0, 'attachment', 'image/jpeg', 0),
(13, 1, '2018-01-16 21:16:56', '2018-01-16 23:16:56', '', 'blog1', '', 'inherit', 'open', 'closed', '', 'blog1', '', '', '2018-01-16 21:16:56', '2018-01-16 23:16:56', '', 7, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/blog1.png', 0, 'attachment', 'image/png', 0),
(14, 1, '2018-01-16 21:17:00', '2018-01-16 23:17:00', '', 'blog3', '', 'inherit', 'open', 'closed', '', 'blog3-2', '', '', '2018-01-16 21:17:00', '2018-01-16 23:17:00', '', 7, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/blog3.png', 0, 'attachment', 'image/png', 0),
(15, 1, '2018-01-16 21:17:08', '2018-01-16 23:17:08', '', 'blogSingle', '', 'inherit', 'open', 'closed', '', 'blogsingle', '', '', '2018-01-16 21:17:08', '2018-01-16 23:17:08', '', 7, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/blogSingle.png', 0, 'attachment', 'image/png', 0),
(16, 1, '2018-01-16 21:17:14', '2018-01-16 23:17:14', '', 'blog', '', 'inherit', 'open', 'closed', '', 'blog', '', '', '2018-01-16 21:17:14', '2018-01-16 23:17:14', '', 7, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/blog.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2018-01-16 21:17:59', '2018-01-16 23:17:59', '', 'Destaque 1', '', 'publish', 'closed', 'closed', '', 'destaque-1', '', '', '2018-01-16 21:18:00', '2018-01-16 23:18:00', '', 0, 'http://localhost/projetos/nexcore_site/?post_type=destaque&#038;p=17', 0, 'destaque', '', 0),
(18, 1, '2018-01-16 21:17:55', '2018-01-16 23:17:55', '', 'blog', '', 'inherit', 'open', 'closed', '', 'blog-2', '', '', '2018-01-16 21:17:55', '2018-01-16 23:17:55', '', 17, 'http://localhost/projetos/nexcore_site/wp-content/uploads/2018/01/blog.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2018-01-18 21:06:03', '2018-01-18 23:06:03', '', 'Destaque 2', '', 'publish', 'closed', 'closed', '', 'destaque-2', '', '', '2018-01-18 21:06:03', '2018-01-18 23:06:03', '', 0, 'http://localhost/projetos/nexcore_site/?post_type=destaque&#038;p=23', 0, 'destaque', '', 0),
(24, 1, '2018-01-18 21:18:54', '2018-01-18 23:18:54', '', 'Destaque 4', '', 'publish', 'closed', 'closed', '', 'destaque-4', '', '', '2018-01-18 21:18:54', '2018-01-18 23:18:54', '', 0, 'http://localhost/projetos/nexcore_site/?post_type=destaque&#038;p=24', 0, 'destaque', '', 0),
(25, 1, '2018-01-18 21:26:06', '2018-01-18 23:26:06', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 3', '', 'publish', 'open', 'open', '', 'post-3', '', '', '2018-01-18 21:26:46', '2018-01-18 23:26:46', '', 0, 'http://localhost/projetos/nexcore_site/?p=25', 0, 'post', '', 0),
(26, 1, '2018-01-18 21:26:06', '2018-01-18 23:26:06', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 3', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2018-01-18 21:26:06', '2018-01-18 23:26:06', '', 25, 'http://localhost/projetos/nexcore_site/2018/01/18/25-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2018-01-20 22:32:01', '2018-01-21 00:32:01', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-01-20 22:32:01', '2018-01-21 00:32:01', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/logo.png', 0, 'attachment', 'image/png', 0),
(28, 1, '2018-01-20 22:32:47', '2018-01-21 00:32:47', '', 'logoRodape', '', 'inherit', 'open', 'closed', '', 'logorodape', '', '', '2018-01-20 22:32:47', '2018-01-21 00:32:47', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/logoRodape.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2018-01-20 22:44:35', '2018-01-21 00:44:35', '', 'Home', '', 'publish', 'closed', 'closed', '', 'inicio', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=29', 1, 'nav_menu_item', '', 0),
(37, 1, '2018-01-20 23:16:15', '2018-01-21 01:16:15', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 4', '', 'publish', 'open', 'open', '', 'post-4', '', '', '2018-01-21 22:47:36', '2018-01-22 00:47:36', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=37', 0, 'post', '', 0),
(31, 1, '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 'A nexcore', '', 'publish', 'closed', 'closed', '', 'a-nexcore', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=31', 2, 'nav_menu_item', '', 0),
(32, 1, '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 'Soluções', '', 'publish', 'closed', 'closed', '', 'solucoes', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=32', 3, 'nav_menu_item', '', 0),
(33, 1, '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 'Produtos', '', 'publish', 'closed', 'closed', '', 'produtos', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=33', 4, 'nav_menu_item', '', 0),
(34, 1, '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=34', 5, 'nav_menu_item', '', 0),
(35, 1, '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=35', 6, 'nav_menu_item', '', 0),
(36, 1, '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2018-01-20 22:47:28', '2018-01-21 00:47:28', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=36', 7, 'nav_menu_item', '', 0),
(38, 1, '2018-01-20 23:15:55', '2018-01-21 01:15:55', '', 'diary-968603_960_720', '', 'inherit', 'open', 'closed', '', 'diary-968603_960_720', '', '', '2018-01-20 23:15:55', '2018-01-21 01:15:55', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/diary-968603_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2018-01-20 23:15:57', '2018-01-21 01:15:57', '', 'home-office-336378_960_720', '', 'inherit', 'open', 'closed', '', 'home-office-336378_960_720', '', '', '2018-01-20 23:15:57', '2018-01-21 01:15:57', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/home-office-336378_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2018-01-20 23:15:59', '2018-01-21 01:15:59', '', 'movement-2203657_960_720', '', 'inherit', 'open', 'closed', '', 'movement-2203657_960_720', '', '', '2018-01-20 23:15:59', '2018-01-21 01:15:59', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/movement-2203657_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2018-01-20 23:16:01', '2018-01-21 01:16:01', '', 'office-581131_960_720', '', 'inherit', 'open', 'closed', '', 'office-581131_960_720', '', '', '2018-01-20 23:16:01', '2018-01-21 01:16:01', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/office-581131_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2018-01-20 23:16:02', '2018-01-21 01:16:02', '', 'statistic-1820320_960_720', '', 'inherit', 'open', 'closed', '', 'statistic-1820320_960_720', '', '', '2018-01-20 23:16:02', '2018-01-21 01:16:02', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/statistic-1820320_960_720.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2018-01-20 23:16:05', '2018-01-21 01:16:05', '', 'workspace-766045_960_720', '', 'inherit', 'open', 'closed', '', 'workspace-766045_960_720', '', '', '2018-01-20 23:16:05', '2018-01-21 01:16:05', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/wp-content/uploads/2018/01/workspace-766045_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(44, 1, '2018-01-20 23:16:15', '2018-01-21 01:16:15', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 4', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2018-01-20 23:16:15', '2018-01-21 01:16:15', '', 37, 'http://localhost/htdocs/projetos/nexcore_blog/37-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2018-01-20 23:16:51', '2018-01-21 01:16:51', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 5', '', 'publish', 'open', 'open', '', 'post-5', '', '', '2018-01-21 22:47:32', '2018-01-22 00:47:32', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=45', 0, 'post', '', 0),
(46, 1, '2018-01-20 23:16:51', '2018-01-21 01:16:51', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 5', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2018-01-20 23:16:51', '2018-01-21 01:16:51', '', 45, 'http://localhost/htdocs/projetos/nexcore_blog/45-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2018-01-20 23:17:18', '2018-01-21 01:17:18', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 6', '', 'publish', 'open', 'open', '', 'post-6', '', '', '2018-01-21 22:47:27', '2018-01-22 00:47:27', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=47', 0, 'post', '', 0),
(48, 1, '2018-01-20 23:17:18', '2018-01-21 01:17:18', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 6', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-01-20 23:17:18', '2018-01-21 01:17:18', '', 47, 'http://localhost/htdocs/projetos/nexcore_blog/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-01-20 23:17:46', '2018-01-21 01:17:46', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 6', '', 'publish', 'open', 'open', '', 'post-6-2', '', '', '2018-01-21 22:47:24', '2018-01-22 00:47:24', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=49', 0, 'post', '', 0);
INSERT INTO `nx_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(50, 1, '2018-01-20 23:17:46', '2018-01-21 01:17:46', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 6', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-01-20 23:17:46', '2018-01-21 01:17:46', '', 49, 'http://localhost/htdocs/projetos/nexcore_blog/49-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-01-20 23:18:38', '2018-01-21 01:18:38', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 7', '', 'publish', 'open', 'open', '', 'post-7', '', '', '2018-01-29 09:35:59', '2018-01-29 11:35:59', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=51', 0, 'post', '', 0),
(52, 1, '2018-01-20 23:18:38', '2018-01-21 01:18:38', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 7', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2018-01-20 23:18:38', '2018-01-21 01:18:38', '', 51, 'http://localhost/htdocs/projetos/nexcore_blog/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2018-01-20 23:19:15', '2018-01-21 01:19:15', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 8', '', 'publish', 'open', 'open', '', 'post-8', '', '', '2018-01-21 22:47:16', '2018-01-22 00:47:16', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=53', 0, 'post', '', 0),
(54, 1, '2018-01-20 23:19:15', '2018-01-21 01:19:15', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 8', '', 'inherit', 'closed', 'closed', '', '53-revision-v1', '', '', '2018-01-20 23:19:15', '2018-01-21 01:19:15', '', 53, 'http://localhost/htdocs/projetos/nexcore_blog/53-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2018-01-20 23:19:40', '2018-01-21 01:19:40', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 9', '', 'publish', 'open', 'open', '', 'post-9', '', '', '2018-01-29 09:36:02', '2018-01-29 11:36:02', '', 0, 'http://localhost/htdocs/projetos/nexcore_blog/?p=55', 0, 'post', '', 0),
(56, 1, '2018-01-20 23:19:40', '2018-01-21 01:19:40', 'A experiência do cliente está diretamente relacionada ao atendimento ao cliente. É a soma das experiências do cliente com uma empresa durante todo o período de seu relacionamento. Integer enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum leo porta non pulvinar neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit amet facilisis magna etiam tempor.\r\n\r\nOrci eu lobortis elementum nibh tellus molestie nunc non blandit massa enim nec dui mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor massa idque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdie malesuada pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas dui id arcu odio ut sem nulla pharetra diam sit amet nisl suscipit adipiscing bibendum integer quis auctor elit sed vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc interdum varius sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus dictum sit amet justo donec enim diam vulputate ut pharetra sit amet.\r\n\r\nAliquam id diam maecenas ultricies mi eget mauris pharetra et ultrices neque ornare euismod elementum nisi quis eleifend quam adipiscing vitae proin sagittis nisl rhoncus rhoncus urna neque viverra justo nec ultrices dui sapien eget mi proin sed libero enim faucibus turpis in eu mi bibendum neque egestas congue quisque egestas diam in arcu euismod quis viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat sed cras arcu dui vivamus arcu felis bibendum ut tristique et egestas quis ipsum suspendisse gravida dictum fusce ut rat orci nulla pellentesque dignissim enim sit amet urna cursus eget nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt interdum velit euismod in pellentesque massa placerat duis ultricies lacus sed turpis id aliquet risus feugiat in ante metus dictum at tempor commodo ullamcorper a lacus sed arcu non odio euismod lacinia at quis risus sed vulputate.\r\n\r\nOdio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scele dictum non consectetur a erat nam at lectus urna duis convallis convallis tellus id inter laoreet id donec ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nis mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci ac au augue mauris augue neque gravida in fermentum et sollicitudin ac orci phasellus egestas t rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum odi feugiat pretium nibh ipsum consequat nisl vel pretium lectus quam id leo in vitae turpis sed elementum tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse i est ante in nibh mauris cursus mattis.', 'Post 9', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2018-01-20 23:19:40', '2018-01-21 01:19:40', '', 55, 'http://localhost/htdocs/projetos/nexcore_blog/55-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2018-01-29 09:30:56', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-01-29 09:30:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/nexcore_site/?p=57', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_termmeta`
--

DROP TABLE IF EXISTS `nx_termmeta`;
CREATE TABLE IF NOT EXISTS `nx_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_terms`
--

DROP TABLE IF EXISTS `nx_terms`;
CREATE TABLE IF NOT EXISTS `nx_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_terms`
--

INSERT INTO `nx_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Outros', 'outros', 0),
(2, 'Call Center', 'call-center', 0),
(3, 'Ta', 'ta', 0),
(4, 'Nuvem', 'nuvem', 0),
(5, 'Produtividade', 'produtividade', 0),
(6, 'PABX', 'pabx', 0),
(7, 'Mobilidade', 'mobilidade', 0),
(8, 'Call Center', 'call-center', 0),
(9, 'URA', 'ura', 0),
(10, 'N Cal', 'n-cal', 0),
(11, 'SMS', 'sms', 0),
(12, 'Mail', 'mail', 0),
(13, 'VOIP', 'voip', 0),
(14, 'Nexcore', 'nexcore', 0),
(15, 'Cloud', 'cloud', 0),
(16, 'URA Reversa', 'ura-reversa', 0),
(17, 'Menu Principal', 'menu-principal', 0),
(18, 'Similares', 'similares', 0),
(19, 'E-BOOK\'S GRATUITOS', 'e-books-gratuitos', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_term_relationships`
--

DROP TABLE IF EXISTS `nx_term_relationships`;
CREATE TABLE IF NOT EXISTS `nx_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_term_relationships`
--

INSERT INTO `nx_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(25, 1, 0),
(7, 2, 0),
(7, 7, 0),
(7, 6, 0),
(7, 5, 0),
(7, 4, 0),
(7, 8, 0),
(7, 9, 0),
(7, 10, 0),
(7, 11, 0),
(7, 12, 0),
(7, 13, 0),
(7, 14, 0),
(7, 15, 0),
(7, 16, 0),
(29, 17, 0),
(37, 1, 0),
(31, 17, 0),
(32, 17, 0),
(33, 17, 0),
(34, 17, 0),
(35, 17, 0),
(36, 17, 0),
(37, 2, 0),
(37, 7, 0),
(37, 4, 0),
(37, 6, 0),
(37, 5, 0),
(49, 2, 0),
(45, 5, 0),
(47, 2, 0),
(47, 7, 0),
(49, 7, 0),
(51, 1, 0),
(51, 4, 0),
(55, 18, 0),
(53, 7, 0),
(55, 2, 0),
(53, 18, 0),
(51, 18, 0),
(49, 18, 0),
(47, 18, 0),
(45, 18, 0),
(37, 18, 0),
(51, 19, 0),
(55, 19, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_term_taxonomy`
--

DROP TABLE IF EXISTS `nx_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `nx_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_term_taxonomy`
--

INSERT INTO `nx_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'category', '', 0, 5),
(3, 3, 'post_tag', '', 0, 0),
(4, 4, 'category', '', 0, 3),
(5, 5, 'category', '', 0, 3),
(6, 6, 'category', '', 0, 2),
(7, 7, 'category', '', 0, 5),
(8, 8, 'post_tag', '', 0, 1),
(9, 9, 'post_tag', '', 0, 1),
(10, 10, 'post_tag', '', 0, 1),
(11, 11, 'post_tag', '', 0, 1),
(12, 12, 'post_tag', '', 0, 1),
(13, 13, 'post_tag', '', 0, 1),
(14, 14, 'post_tag', '', 0, 1),
(15, 15, 'post_tag', '', 0, 1),
(16, 16, 'post_tag', '', 0, 1),
(17, 17, 'nav_menu', '', 0, 7),
(18, 18, 'category', '', 0, 7),
(19, 19, 'category', '', 0, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_usermeta`
--

DROP TABLE IF EXISTS `nx_usermeta`;
CREATE TABLE IF NOT EXISTS `nx_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_usermeta`
--

INSERT INTO `nx_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'nexcore'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'nx_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'nx_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"f2ab07653626fcfa30dfdf81f643972dea2fbbfa1a599f819bec729efcad1de4\";a:4:{s:10:\"expiration\";i:1517398253;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\";s:5:\"login\";i:1517225453;}}'),
(17, 1, 'nx_dashboard_quick_press_last_post_id', '57'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(19, 1, 'nx_yoast_notifications', 'a:2:{i:0;a:2:{s:7:\"message\";s:181:\"Don\'t miss your crawl errors: <a href=\"http://localhost/projetos/nexcore_site/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">connect with Google Search Console here</a>.\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:1;a:2:{s:7:\"message\";s:235:\"<strong>Huge SEO Issue: You\'re blocking access to robots.</strong> You must <a href=\"http://localhost/projetos/nexcore_site/wp-admin/options-reading.php\">go to your Reading Settings</a> and uncheck the box for Search Engine Visibility.\";s:7:\"options\";a:8:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:32:\"wpseo-dismiss-blog-public-notice\";s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";}}}'),
(36, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(37, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:22:\"add-post-type-destaque\";i:1;s:12:\"add-post_tag\";}'),
(20, 1, 'closedpostboxes_post', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(21, 1, 'metaboxhidden_post', 'a:7:{i:0;s:11:\"postexcerpt\";i:1;s:13:\"trackbacksdiv\";i:2;s:10:\"postcustom\";i:3;s:16:\"commentstatusdiv\";i:4;s:7:\"slugdiv\";i:5;s:9:\"authordiv\";i:6;s:11:\"commentsdiv\";}'),
(22, 1, 'meta-box-order_post', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:38:\"submitdiv,categorydiv,tagsdiv-post_tag\";s:6:\"normal\";s:94:\"metaboxPost,wpseo_meta,postexcerpt,trackbacksdiv,postcustom,commentstatusdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(23, 1, 'screen_layout_post', '2'),
(24, 1, 'nx_user-settings', 'editor=tinymce&libraryContent=browse'),
(25, 1, 'nx_user-settings-time', '1516144640'),
(26, 1, '_yoast_wpseo_profile_updated', '1516144544'),
(31, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(27, 1, 'meta-box-order_destaque', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:34:\"metaboxDestaque,wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(28, 1, 'screen_layout_destaque', '2'),
(29, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(30, 1, 'metaboxhidden_destaque', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(32, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:24:\"wpseo-dashboard-overview\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(35, 1, 'last_login_time', '2018-01-29 09:30:53'),
(33, 1, 'closedpostboxes_servico', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(34, 1, 'metaboxhidden_servico', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_users`
--

DROP TABLE IF EXISTS `nx_users`;
CREATE TABLE IF NOT EXISTS `nx_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_users`
--

INSERT INTO `nx_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'nexcore', '$P$BefQTpFf5MGCiyU8XVEk5zIimHlbqc1', 'nexcore', 'contato@hudsoncarolino.com.br', '', '2018-01-16 22:51:34', '', 0, 'nexcore');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_campaign`
--

DROP TABLE IF EXISTS `nx_wysija_campaign`;
CREATE TABLE IF NOT EXISTS `nx_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`campaign_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_campaign`
--

INSERT INTO `nx_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guia de Usuário de 5 Minutos', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_campaign_list`
--

DROP TABLE IF EXISTS `nx_wysija_campaign_list`;
CREATE TABLE IF NOT EXISTS `nx_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text,
  PRIMARY KEY (`list_id`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_custom_field`
--

DROP TABLE IF EXISTS `nx_wysija_custom_field`;
CREATE TABLE IF NOT EXISTS `nx_wysija_custom_field` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_email`
--

DROP TABLE IF EXISTS `nx_wysija_email`;
CREATE TABLE IF NOT EXISTS `nx_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext,
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_email`
--

INSERT INTO `nx_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guia de Usuário de 5 Minutos', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"  >\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n    <title>Guia de Usuário de 5 Minutos</title>\n    <style type=\"text/css\">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: \"Arial\";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:\"Arial\";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: \"Arial\" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^=\"tel\"], a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^=\"tel\"],\n        a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type=\"text/css\">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type=\"text/css\">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type=\"text/css\">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor=\"#e8e8e8\" yahoo=\"fix\">\n    <span style=\"margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;\">\n    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"wysija_wrapper\">\n        <tr>\n            <td valign=\"top\" align=\"center\">\n                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            <p class=\"wysija_viewbrowser_container\" style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" >Problemas de visualização? <a style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[view_in_browser_link]\" target=\"_blank\">Veja esta newsletter em seu navegador.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\">\n                            \n<table class=\"wysija_header\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_header_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"72\" src=\"http://localhost/projetos/nexcore_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"left\">\n                            \n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 1:</strong> ei, clique neste texto!</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Para editar, simplesmente clique neste bloco de texto.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/nexcore_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 2:</strong> brinque com esta imagem</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n \n \n <table style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;\" width=\"1%\" height=\"190\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td class=\"wysija_image_container left\" style=\"border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;\" width=\"1%\" height=\"190\" valign=\"top\">\n <div align=\"left\" class=\"wysija_image_placeholder left\" style=\"height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\" >\n \n <img width=\"281\" height=\"190\" src=\"http://localhost/projetos/nexcore_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class=\"wysija_text_container\"><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Posicione o seu mouse acima da imagem à esquerda.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/nexcore_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 3:</strong> solte conteúdo aqui</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Arraste e solte <strong>texto, posts, divisores.</strong> Veja no lado direito!</p><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Você pode até criar <strong>compartilhamentos sociais</strong> como estes:</p></div>\n </td>\n \n </tr>\n</table>\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n <td class=\"wysija_gallery_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" >\n <table class=\"wysija_gallery_table center\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;\" width=\"184\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.facebook.com/mailpoetplugin\"><img src=\"http://localhost/projetos/nexcore_site/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png\" border=\"0\" alt=\"Facebook\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.twitter.com/mail_poet\"><img src=\"http://localhost/projetos/nexcore_site/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png\" border=\"0\" alt=\"Twitter\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"https://plus.google.com/+Mailpoet\"><img src=\"http://localhost/projetos/nexcore_site/wp-content/uploads/wysija/bookmarks/medium/02/google.png\" border=\"0\" alt=\"Google\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://localhost/projetos/nexcore_site/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 4:</strong> e o rodapé?</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Modifique o conteúdo do rodapé na <strong>página de opções</strong> do MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            \n<table class=\"wysija_footer\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_footer_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"46\" src=\"http://localhost/projetos/nexcore_site/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"  >\n                            <p class=\"wysija_unsubscribe_container\" style=\"font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" ><a style=\"color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[unsubscribe_link]\" target=\"_blank\">Cancelar Assinatura</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1516143372, 1516143372, NULL, 'info@localhost', 'nexcore', 'info@localhost', 'nexcore', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEyMToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9uZXhjb3JlX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLnBuZyI7czo5OiJ0aHVtYl91cmwiO3M6MTI5OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL25leGNvcmVfc2l0ZS93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24tMTUweDE1MC5wbmciO319fQ==', 'YTo0OntzOjc6InZlcnNpb24iO3M6NToiMi44LjEiO3M6NjoiaGVhZGVyIjthOjU6e3M6NDoidGV4dCI7TjtzOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyMToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9uZXhjb3JlX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvaGVhZGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NzI7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImhlYWRlciI7fXM6NDoiYm9keSI7YTo5OntzOjc6ImJsb2NrLTEiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTYwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNVG84TDNOMGNtOXVaejRnWldrc0lHTnNhWEYxWlNCdVpYTjBaU0IwWlhoMGJ5RThMMmd5UGp4d1BsQmhjbUVnWldScGRHRnlMQ0J6YVcxd2JHVnpiV1Z1ZEdVZ1kyeHBjWFZsSUc1bGMzUmxJR0pzYjJOdklHUmxJSFJsZUhSdkxqd3ZjRDQ9Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjgzOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL25leGNvcmVfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay0zIjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjgwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNam84TDNOMGNtOXVaejRnWW5KcGJuRjFaU0JqYjIwZ1pYTjBZU0JwYldGblpXMDhMMmd5UGc9PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjM7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTQiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6NzY6IlBIQStVRzl6YVdOcGIyNWxJRzhnYzJWMUlHMXZkWE5sSUdGamFXMWhJR1JoSUdsdFlXZGxiU0REb0NCbGMzRjFaWEprWVM0OEwzQSsiO31zOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyMToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9uZXhjb3JlX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLnBuZyI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7fXM6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo0O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay01IjthOjU6e3M6ODoicG9zaXRpb24iO2k6NTtzOjQ6InR5cGUiO3M6NzoiZGl2aWRlciI7czozOiJzcmMiO3M6ODM6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvbmV4Y29yZV9zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTYiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MzAwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNem84TDNOMGNtOXVaejRnYzI5c2RHVWdZMjl1ZEdYRHVtUnZJR0Z4ZFdrOEwyZ3lQanh3UGtGeWNtRnpkR1VnWlNCemIyeDBaU0E4YzNSeWIyNW5QblJsZUhSdkxDQndiM04wY3l3Z1pHbDJhWE52Y21Wekxqd3ZjM1J5YjI1blBpQldaV3BoSUc1dklHeGhaRzhnWkdseVpXbDBieUU4TDNBK1BIQStWbTlqdzZvZ2NHOWtaU0JoZE1PcElHTnlhV0Z5SUR4emRISnZibWMrWTI5dGNHRnlkR2xzYUdGdFpXNTBiM01nYzI5amFXRnBjend2YzNSeWIyNW5QaUJqYjIxdklHVnpkR1Z6T2p3dmNEND0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo2O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay03IjthOjU6e3M6NToid2lkdGgiO2k6MTg0O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo1OiJpdGVtcyI7YTozOntpOjA7YTo3OntzOjM6InVybCI7czozODoiaHR0cDovL3d3dy5mYWNlYm9vay5jb20vbWFpbHBvZXRwbHVnaW4iO3M6MzoiYWx0IjtzOjg6IkZhY2Vib29rIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjk3OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL25leGNvcmVfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZmFjZWJvb2sucG5nIjtzOjU6IndpZHRoIjtpOjMyO3M6NjoiaGVpZ2h0IjtpOjMyO31pOjE7YTo3OntzOjM6InVybCI7czozMjoiaHR0cDovL3d3dy50d2l0dGVyLmNvbS9tYWlsX3BvZXQiO3M6MzoiYWx0IjtzOjc6IlR3aXR0ZXIiO3M6OToiY2VsbFdpZHRoIjtpOjYxO3M6MTA6ImNlbGxIZWlnaHQiO2k6MzI7czozOiJzcmMiO3M6OTY6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvbmV4Y29yZV9zaXRlL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi90d2l0dGVyLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9aToyO2E6Nzp7czozOiJ1cmwiO3M6MzM6Imh0dHBzOi8vcGx1cy5nb29nbGUuY29tLytNYWlscG9ldCI7czozOiJhbHQiO3M6NjoiR29vZ2xlIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjk1OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL25leGNvcmVfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjgzOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL25leGNvcmVfc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay05IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjE4ODoiUEdneVBqeHpkSEp2Ym1jK1VHRnpjMjhnTkRvOEwzTjBjbTl1Wno0Z1pTQnZJSEp2WkdGd3c2ay9QQzlvTWo0OGNENU5iMlJwWm1seGRXVWdieUJqYjI1MFpjTzZaRzhnWkc4Z2NtOWtZWEREcVNCdVlTQThjM1J5YjI1blBuRERvV2RwYm1FZ1pHVWdiM0REcDhPMVpYTThMM04wY205dVp6NGdaRzhnVFdGcGJGQnZaWFF1UEM5d1BnPT0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo5O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9fXM6NjoiZm9vdGVyIjthOjU6e3M6NDoidGV4dCI7TjtzOjU6ImltYWdlIjthOjU6e3M6Mzoic3JjIjtzOjEyMToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9uZXhjb3JlX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvZm9vdGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NDY7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImZvb3RlciI7fX0=', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirme sua assinatura em Nexcore', 'Olá!\n\nLegal! Você se inscreveu como assinante em nosso site.\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \n\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\n\nObrigado,\n\nA equipe!\n', 1516143373, 1516143373, NULL, 'info@localhost', 'nexcore', 'info@localhost', 'nexcore', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_email_user_stat`
--

DROP TABLE IF EXISTS `nx_wysija_email_user_stat`;
CREATE TABLE IF NOT EXISTS `nx_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_email_user_url`
--

DROP TABLE IF EXISTS `nx_wysija_email_user_url`;
CREATE TABLE IF NOT EXISTS `nx_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_form`
--

DROP TABLE IF EXISTS `nx_wysija_form`;
CREATE TABLE IF NOT EXISTS `nx_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_form`
--

INSERT INTO `nx_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Assine nossa Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjczOiJWZWphIHN1YSBjYWl4YSBkZSBlbnRyYWRhIG91IHBhc3RhIGRlIHNwYW0gcGFyYSBjb25maXJtYXIgc3VhIGFzc2luYXR1cmEuIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjE3OiJsaXN0c19zZWxlY3RlZF9ieSI7czo1OiJhZG1pbiI7fXM6NDoiYm9keSI7YToyOntpOjA7YTo0OntzOjQ6Im5hbWUiO3M6NToiRW1haWwiO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo2OiJwYXJhbXMiO2E6Mjp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czo4OiJyZXF1aXJlZCI7YjoxO319aToxO2E6NDp7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJmaWVsZCI7czo2OiJzdWJtaXQiO3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6ODoiQXNzaW5hciEiO319fXM6NzoiZm9ybV9pZCI7aToxO30=', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_list`
--

DROP TABLE IF EXISTS `nx_wysija_list`;
CREATE TABLE IF NOT EXISTS `nx_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_list`
--

INSERT INTO `nx_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'Minha primeira lista', 'minha-primeira-lista', 'A lista criada automaticamente na instalação do MailPoet.', 0, 0, 1, 1, 1516143372, 0),
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : \"WordPress', 0, 0, 0, 0, 1516143373, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_queue`
--

DROP TABLE IF EXISTS `nx_wysija_queue`;
CREATE TABLE IF NOT EXISTS `nx_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`),
  KEY `SENT_AT_INDEX` (`send_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_subscriber_ips`
--

DROP TABLE IF EXISTS `nx_wysija_subscriber_ips`;
CREATE TABLE IF NOT EXISTS `nx_wysija_subscriber_ips` (
  `ip` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`created_at`,`ip`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_url`
--

DROP TABLE IF EXISTS `nx_wysija_url`;
CREATE TABLE IF NOT EXISTS `nx_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `url` text,
  PRIMARY KEY (`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_url_mail`
--

DROP TABLE IF EXISTS `nx_wysija_url_mail`;
CREATE TABLE IF NOT EXISTS `nx_wysija_url_mail` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_user`
--

DROP TABLE IF EXISTS `nx_wysija_user`;
CREATE TABLE IF NOT EXISTS `nx_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL_UNIQUE` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_user`
--

INSERT INTO `nx_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'contato@hudsoncarolino.com.br', '', '', '', '0', NULL, NULL, NULL, '', 1516143374, 1, 'hudsoncarolino.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_user_field`
--

DROP TABLE IF EXISTS `nx_wysija_user_field`;
CREATE TABLE IF NOT EXISTS `nx_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_user_field`
--

INSERT INTO `nx_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_user_history`
--

DROP TABLE IF EXISTS `nx_wysija_user_history`;
CREATE TABLE IF NOT EXISTS `nx_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_wysija_user_list`
--

DROP TABLE IF EXISTS `nx_wysija_user_list`;
CREATE TABLE IF NOT EXISTS `nx_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`list_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nx_wysija_user_list`
--

INSERT INTO `nx_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1516143372, 0),
(2, 1, 1516143373, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_yoast_seo_links`
--

DROP TABLE IF EXISTS `nx_yoast_seo_links`;
CREATE TABLE IF NOT EXISTS `nx_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nx_yoast_seo_meta`
--

DROP TABLE IF EXISTS `nx_yoast_seo_meta`;
CREATE TABLE IF NOT EXISTS `nx_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `nx_yoast_seo_meta`
--

INSERT INTO `nx_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(1, 0, 0),
(7, 0, 0),
(17, 0, 0),
(23, 0, 0),
(24, 0, 0),
(25, 0, 0),
(30, 0, 0),
(37, 0, 0),
(45, 0, 0),
(47, 0, 0),
(49, 0, 0),
(51, 0, 0),
(53, 0, 0),
(55, 0, 0),
(10, 0, 0),
(3, 0, 0),
(9, 0, 0),
(19, 0, 0),
(20, 0, 0),
(21, 0, 0),
(22, 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
