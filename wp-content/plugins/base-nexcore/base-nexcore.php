<?php
/**
 * Plugin Name: Base Nexcore
 * Description: Controle base do tema Nexcore.
 * Version: 0.1
 * Author: Hudson Caronilo
 * Author URI: 
 * Licence: GPL2
 */

	function baseNexcore () {

		//TIPOS DE CONTEÚDO
		conteudosNexcore();

		//TAXONOMIA
		taxonomiaNexcore();

		//META BOXES
		metaboxesNexcore();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
		function conteudosNexcore (){

			// TIPOS DE DESTQUE
			tipoDestaque();


			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){

				switch (get_current_screen()->post_type) {

					case 'equipe':
						$titulo = 'Nome do integrante';
					break;

					default:
					break;
				}
			    return $titulo;
			}

		}
	
	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		



	/****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesNexcore(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'Nexcore_';


				//METABOX PÁGINA DE DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxDestaque',
					'title'			=> 'Detalhes do destaque',
					'pages' 		=> array('destaque'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
					
						array(
							'name'  => 'Link do destaque: ',
							'id'    => "{$prefix}destaque_link",
							'desc'  => '',
							'type'  => 'text',
						),

					),
					
				);

				//METABOX PÁGINA DE DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxPost',
					'title'			=> 'Detalhes do post',
					'pages' 		=> array('post'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
					
						array(
							'name'  => 'SubTítulo: ',
							'id'    => "{$prefix}subtitulo_post",
							'desc'  => '',
							'type'  => 'text',
						),

					),
					
				);


				return $metaboxes;
			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaNexcore () {
			// taxonomiaCategoriaProjetos();
		}

		// function taxonomiaCategoriaProjetos() {

		// 	$rotulosCategoriaProjetos = array(
		// 										'name'              => 'Categorias de projeto',
		// 										'singular_name'     => 'Categorias de projetos',
		// 										'search_items'      => 'Buscar categoria do projeto',
		// 										'all_items'         => 'Todas as categorias',
		// 										'parent_item'       => 'Categoria pai',
		// 										'parent_item_colon' => 'Categoria pai:',
		// 										'edit_item'         => 'Editar categoria do projeto',
		// 										'update_item'       => 'Atualizar categoria',
		// 										'add_new_item'      => 'Nova categoria',
		// 										'new_item_name'     => 'Nova categoria',
		// 										'menu_name'         => 'Categorias projetos',
		// 									);

		// 	$argsCategoriaProjetos 		= array(
		// 										'hierarchical'      => true,
		// 										'labels'            => $rotulosCategoriaProjetos,
		// 										'show_ui'           => true,
		// 										'show_admin_column' => true,
		// 										'query_var'         => true,
		// 										'rewrite'           => array( 'slug' => 'categoria-projetos' ),
		// 									);

		// 	register_taxonomy( 'categoriaProjetos', array( 'projetos' ), $argsCategoriaProjetos);

		// }
		// 
		function metaboxjs(){



			global $post;

			$template = get_post_meta($post->ID, '_wp_page_template', true);

			$template = explode('/', $template);

			$template = explode('.', $template[1]);

			$template = $template[0];



			if($template != ''){

				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);

			}

		}


  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'baseNexcore');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {

	    	baseNexcore();

	   		flush_rewrite_rules();
		}

		register_activation_hook( __FILE__, 'rewrite_flush' );