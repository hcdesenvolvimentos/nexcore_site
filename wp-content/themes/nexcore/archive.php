<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Nexcore
 */

get_header(); ?>
<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">
		
		<!-- SESSÃO CARROSSEL DESTAQUE -->
		<section class="sessaocarrosselDestaque">
			<!-- TÍTULO DA SESSÃO  -->
			<h6 class="hidden">Carrossel Destaque</h6>

			<!-- CARROSSEL DESTAQUE -->
			<div class="carrosselDestaque" id="carrosselDestaque">
				<?php 
					//LOOP DE POST DESTAQUES
					$postDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $postDestaques->have_posts() ) : $postDestaques->the_post();

						$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaque = $fotoDestaque[0];
				?>
				<!-- ITEM -->
				<div class="item">
					<img src="<?php echo $fotoDestaque ?>" alt="<?php echo get_the_title() ?> ">
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>

		</section>

        <!-- ÁREA MENU CATEGORIAS E PESQUISA-->
	    <div class="categoriasPesquisa">
			<div class="container">
				<div class="row">

                	<!-- ÁREA DE CATEGORIAS-->
                	<div class="col-md-3 col-sm-6">
            			<div class="menu">
            				<p id="categoriaDrop"> <?php $category = get_the_category(); 
							echo $category[0]->cat_name; ?> 
							<i class="fa fa-angle-double-down" aria-hidden="true"></i></p>
            				<div class="drop dropcategoria">
	        					<?php
									// LISTA DE CATEGORIAS
									$arrayCategorias = array();
									$categorias=get_categories($args);
									foreach($categorias as $categoria):
									$arrayCategorias[$categoria->cat_ID] = $categoria->name;
									$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
								?>
            					<a href="<?php echo get_category_link($categoria->cat_ID); ?>">
            						<h2><?php echo $nomeCategoria; ?></h2>
            					</a>
    							<?php endforeach; ?>
            				</div>
            			</div>
                	</div>

                	<!-- ÁREA DE E-BOOK'S -->
                	<div class="col-md-3 col-sm-6">
                		
                		<div class="menu">
                			<p id="eboock"> E-BOOK'S GRATUITOS <i class="fa fa-angle-double-down" aria-hidden="true"></i></p>
                			<div class="drop eboock">
                				<a href="">
                					<h2>Call Center</h2>
                				</a>
                			</div>
                		</div>
                	</div>

                	<!-- ÁREA DE PESQUISA-->
                	<div class="col-md-6 col-sm-12">
                		<div class="areaPesquisa" id="pesquisa">
                	  		<input type="text" id="campoPesquisa"/>
 							<input type="submit" value="&#xf002">
                        </div>

                	</div>

                </div>

			</div>
	  	</div>

	  	<section class="conteudos">		  		
  			<h6 class="hidden">Sessão de posts</h6>
	  		
	  		<div class="container">

				<div class="row">

					<!-- ÁREA DE POSTS -->
					<div class="col-md-9">
						
						<div class="conteudo">
							<?php 
								//* Start the Loop */
								while ( have_posts() ) : the_post();
									$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoPost = $fotoPost[0];

									
							?>
							<!-- POST -->
							<div class="post">
								<a href=" <?php echo get_permalink() ?> ">
									<div class="row">

										<!-- IMAGEM DO POST -->
										<div class="col-sm-5">
											<div class="imagem">
												<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
											</div>
										</div>

										<!-- DESCRIÇÃO DO POST -->
										<div class="col-sm-7">
											<div class="texto">
												<h2><?php echo get_the_title() ?></h2>
												<span>por <strong> <?php echo $author = get_the_author(); ?> </strong> em <?php the_time('j') ?>/<?php the_time('m') ?>/<?php the_time(' Y') ?></span>
												<p><?php echo customExcerpt(200); ?></p>
											</div>
										</div>

									</div>
								</a>
							</div>	
							<?php endwhile; wp_reset_query(); ?>

						</div>

						<div class="vejaMais">
							<a href="">Veja Mais</a>
						</div>
					</div>

					<!-- ÁREA DE SIDEBAR -->
					<div class="col-md-3">
						<aside>
							
							<div class="praticasSucesso">
								<img src="<?php bloginfo('template_directory'); ?>/img/imgLigacao.png" alt="9 Práticas de Sucesso">
								<h6> 9 Práticas de Sucesso </h6>
								<p>para um Call center mais eficaz</p>
								<a href=""> Saiba como</a>
							</div>

							<div class="assinarBlog">
								<p>Assine nosso <strong>Blog</strong></p>
							    <div class="enviar">
							    	<input type="text" placeholder="Seu e-mail">
							    	<div class="iconeSubmit">										
							    		<input type="submit" value="Enviar">
							    		<i class="fa fa-angle-double-right" aria-hidden="true"></i>
							    	</div>

								</div>
							</div>

						</aside>
					</div>

				</div>

			</div>
		

		</section>			

		<div class="newsllater">
			<div class="container">
				<h6 class="hidden">Assinar o blog para receber notícias</h6>

				<div class="row">

					<!-- ÁREA DE NOTÍCIAS -->
					<div class="col-md-12">
				
						<div class="noticia">
							<h5>Não perca mais nenhum conteúdo!</h5>
							<p>Assine nosso <strong>Blog</strong> e receba novos posts em seu email</p>
							<div class="enviar">
								<!--START Scripts : this is the script part you can add to the header of your theme-->
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-includes/js/jquery/jquery.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
									<script type="text/javascript">
                					
                					/* <![CDATA[ */
               						 var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_home_url() ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
              						  /* ]]> */
               						 </script><script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
									
									<!--END Scripts-->

									<div class="widget_wysija_cont html_wysija">
									<div id="msg-form-wysija-html5a6de662c646d-2" class="wysija-msg ajax"></div>
									<form id="form-wysija-html5a6de662c646d-2" method="post" action="#wysija" class="widget_wysija html_wysija">
								<input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email" placeholder="Coloque aqui seu e-mail" value="" />
    
							    <span class="abs-req">
							        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
							    </span>
							    
									<input class="wysija-submit-field" type="submit" value="Assinar!" />
									<input type="hidden" name="form_id" value="2" />
    									<input type="hidden" name="action" value="save" />
    									<input type="hidden" name="controller" value="subscribers" />
    									<input type="hidden" value="1" name="wysija-page" />
        								<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 										</form>
								</div>
							</div>
						</div>v>

					</div>
						
				</div>

			</div>
			
		</div>

	</div>

<?php
get_footer();
