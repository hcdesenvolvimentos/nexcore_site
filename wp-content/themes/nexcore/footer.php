<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Nexcore
 */

?>
<footer class="rodape">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="areaLogoCOntato">
					<div class="logo">
						<img src="img/logoRodape.png" alt="">
					</div>
					<div class="areaContato">
						<h2>São Paulo</h2>
						<p><i class="fa fa-phone" aria-hidden="true"></i> +55(11) 3010.3288</p>
					</div>
					<div class="areaContato">
						<h2>São Paulo</h2>
						<p><i class="fa fa-phone" aria-hidden="true"></i> +55(11) 3010.3288</p>
					</div>
				</div>
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-5">
						<div class="menuRodape">
							<div class="row">
								<div class="col-sm-3 text-center">
									<div class="nav">
										<a href="http://nexcore.com.br/">Home</a>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="nav">
										<a href="http://nexcore.com.br/empresas">A Nexcore</a>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="nav">
										<a href="">Soluções</a>
										<ul>
											<li><a href="http://nexcore.com.br/solucao/grandes-empresas">Grandes Empresas</a></li><li><a href="http://nexcore.com.br/solucao/orgaos-do-governo">Órgãos do Governo</a></li><li><a href="http://nexcore.com.br/solucao/pequenas-e-medias-empresas">Pequenas e Médias Empresas</a></li>                        <li><a href="http://nexcore.com.br/solucao/aumento-de-produtividade">Aumento de produtividade</a></li><li><a href="http://nexcore.com.br/solucao/contact-center">Contact Center</a></li><li><a href="http://nexcore.com.br/solucao/mobilidade">Mobilidade</a></li><li><a href="http://nexcore.com.br/solucao/pabx">PABX</a></li><li><a href="http://nexcore.com.br/solucao/reducao-de-custos-em-telefonia">Redução de custos em telefonia</a></li><li><a href="http://nexcore.com.br/solucao/telefonia-em-nuvem">Telefonia em nuvem</a></li> 
										</ul>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="menuRodape">
							<div class="row">
								<div class="col-sm-3">
									<div class="nav">
										<a href="">Sistemas</a>
										<ul>
											<li><a href="http://nexcore.com.br/ncall-cloud">Ncall+ cloud</a></li><li><a href="http://nexcore.com.br/ncall-contact-center">Ncall+ Contact Center</a></li><li><a href="http://nexcore.com.br/ncall-gateway">Ncall+ Gateway</a></li><li><a href="http://nexcore.com.br/ncall-voice">Ncall+ Voice</a></li>   
										</ul>
										<strong>Módulos</strong>
										<ul>
											 <li><a href="http://nexcore.com.br/nha">N | H.A</a></li><li><a href="http://nexcore.com.br/nquality">N | QUALITY</a></li><li><a href="http://nexcore.com.br/nsearch">N | SEARCH</a></li><li><a href="http://nexcore.com.br/nsms">N | SMS</a></li><li><a href="http://nexcore.com.br/n-chat">N| Chat</a></li><li><a href="http://nexcore.com.br/n-mail">N| Mail</a></li>   
										</ul>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="nav">
										<a href="http://nexcore.com.br/servicos">Serviços</a>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="nav">
										<a href="<?php echo get_home_url() ?>">Blog</a>
									</div>
								</div>
								<div class="col-sm-5">
									<div class="nav">
										<a href="http://nexcore.com.br/contato">Contato</a>
										<ul>
											<li>
												<a href="http://nexcore.com.br/trabalhe-conosco">Trabalhe conosco</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="segueRedes">
		<p>Siga a Nexcore nas redes</p>
		<a href="https://pt-br.facebook.com/nexcore/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		<a href="https://pt.linkedin.com/company/nexcore-tecnologia" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
