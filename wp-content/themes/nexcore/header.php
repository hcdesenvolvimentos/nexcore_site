<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Nexcore
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<!-- TOPO -->
	<header class="topo">
		<div class="container">
			<div class="row">
				<!-- LOGO -->
				<div class="col-md-4">
					<div class="logo">
						<a href="http://nexcore.com.br/">
						<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="Nexcore" class="img-responsive">
						</a>
					</div>
				</div>

				<!-- MENU -->
				<div class="col-md-8">

					<div class="contato">
						<div class="icone">
							<i class="fa fa-volume-control-phone" aria-hidden="true"></i>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="contatoSP numeros">
									<span><?php echo $configuracao['opt_telefoneSP']?></span>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="contatoCWB numeros">
									<span><?php echo $configuracao['opt_telefoneCWB']?></span>
								</div>
							</div>								
						</div>
						<div class="segueRedes">
							<a href="https://pt-br.facebook.com/nexcore/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="https://pt.linkedin.com/company/nexcore-tecnologia" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="navbar" role="navigation">	
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<ul class="nav navbar-nav">			
									<li><a href="http://nexcore.com.br/" class="" title="Home">Home</a></li>
									<li><a href="http://nexcore.com.br/empresas" class="" title="A nexcore">A nexcore</a></li>
									<li>
										<a href="#" class="" title="Soluções">Soluções</a>
										<ul class="sub">
				                            <li><a href="http://nexcore.com.br/solucao/grandes-empresas">Grandes Empresas</a></li><li><a href="http://nexcore.com.br/solucao/orgaos-do-governo">Órgãos do Governo</a></li><li><a href="http://nexcore.com.br/solucao/pequenas-e-medias-empresas">Pequenas e Médias Empresas</a></li>                            <li><a href="http://nexcore.com.br/solucao/aumento-de-produtividade">Aumento de produtividade</a></li><li><a href="http://nexcore.com.br/solucao/contact-center">Contact Center</a></li><li><a href="http://nexcore.com.br/solucao/mobilidade">Mobilidade</a></li><li><a href="http://nexcore.com.br/solucao/pabx">PABX</a></li><li><a href="http://nexcore.com.br/solucao/reducao-de-custos-em-telefonia">Redução de custos em telefonia</a></li><li><a href="http://nexcore.com.br/solucao/telefonia-em-nuvem">Telefonia em nuvem</a></li>                        
				                        </ul>
									</li>
									<li>
										<a href="#" class="" title="Produtos">Produtos</a>
										<div class="sub sub-produtos"> 
                                        <ul>
	                                        <li><span>Sistemas</span></li>
	                                        <li><a href="ncall-cloud">Ncall+ cloud</a></li><li><a href="ncall-contact-center">Ncall+ Contact Center</a></li><li><a href="ncall-gateway">Ncall+ Gateway</a></li><li><a href="ncall-voice">Ncall+ Voice</a></li> 
	                                        <li class="compare_menu"><a href="compare">Compare</a></li> 
                                    	</ul>
                                    	<ul>
	                                        <li><span>Módulos</span></li>
	                                        <li><a href="nha">N | H.A</a></li><li><a href="nquality">N | QUALITY</a></li><li><a href="nsearch">N | SEARCH</a></li><li><a href="nsms">N | SMS</a></li><li><a href="n-chat">N| Chat</a></li><li><a href="n-mail">N| Mail</a></li>                                    </ul>
                        				</div>
									</li>
									<li><a href="http://nexcore.com.br/servicos" class="" title="Serviços">Serviços</a></li>
									<li><a href="<?php echo get_home_url() ?>" class="" title="Blog">Blog</a></li>
									<li><a href="http://nexcore.com.br/contato" class="" title="Contato">Contato</a></li>
								</ul>
							</nav>		
						</div>			
					</div>
				</div>

			</div>
		</div>
		
	</header>

	<script>
		$(".navbar-collapse>ul").addClass('nav navbar-nav');
		$(".navbar-collapse>ul li a").addClass('scrollTop');
		$("#collapse > div").removeClass('nav navbar-nav');
	</script>