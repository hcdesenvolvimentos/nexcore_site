
$(function(){

	$(".categoriasPesquisa .menu p").click(function(e) {
  		$(this).next().slideToggle();
  		let idCategoria = $(this).attr("id");

  		 if (idCategoria == "categoriaDrop") {
  			$(".eboock").slideUp();
  			console.log(idCategoria);
  		}else if (idCategoria == "eboock") {
  		 	$(".dropcategoria").slideUp();
  			
  		 }
	});

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {

		// SCRIPT CARROSSEL DESTAQUE
		$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			animateOut: 'fadeOut',
		});

		// BOTÕES DO CARROSSEL
		var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
		$('#btnCarrosselDestaqueLeft').click(function(){ carrossel_destaque.prev(); });
		$('#btnCarrosselDestaqueRight').click(function(){ carrossel_destaque.next(); });

		//CARROSEL
		$("#carrosselLogos").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:2
	            },
	           
	            991:{
	                items:3
	            },
	            1024:{
	                items:4
	            },
	            1440:{
	                items:4
	            },
	            			            
	        }	
		});
		
	});

	/*****************************************
		SCRIPTS PÁGINA  POST
	*******************************************/
	$(document).ready(function() {

		// SCRIPT CARROSSEL DE POSTS
		$("#carrosselPostSimilares").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:2
	            },
	           
	            991:{
	                items:3
	            },
	            1024:{
	                items:4
	            },
	            1440:{
	                items:4
	            },
	            			            
	        }	
		});

		// BOTÕES DO CARROSSEL
		var carrossel_post = $("#carrosselPostSimilares").data('owlCarousel');
		$('#btnCarrosselPostSimilaresLeft').click(function(){ carrossel_post.prev(); });
		$('#btCcarrosselPostSimilaresRight').click(function(){ carrossel_post.next(); });

		
		
	});
	
});
