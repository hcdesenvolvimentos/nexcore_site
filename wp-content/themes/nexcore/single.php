<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Nexcore
 */
$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoPost = $fotoPost[0];
global $wp;
$current_url = home_url(add_query_arg(array(),$wp->request));
$subtitulo_post = rwmb_meta('Nexcore_subtitulo_post');
get_header(); ?>
<!-- PÁGINA POST -->
<div class="pg pg-post" id="topo">
	
	<!-- SESSÃO DO BANNER DESTAQUE -->
	<section class="sessaoBannerDestaque">
		
		<!-- TÍTULO DA SESSÃO  -->
		<h6 class="hidden"><?php echo get_the_title() ?></h6>

		<!-- BANNER DESTAQUE -->
        <figure class="bannerDestaque">
            <img src="<?php echo $fotoPost  ?>" alt="<?php echo get_the_title() ?>">
        </figure>

	</section>

    <!-- ÁREA MENU CATEGORIAS E PESQUISA-->
    <div class="categoriasPesquisa" >
		<div class="container">
			<div class="row">

            	<!-- ÁREA DE CATEGORIAS-->
            	<div class="col-md-3 col-sm-6">
        			<div class="menu">
        				<p id="categoriaDrop"> CATEGORIAS <i class="fa fa-angle-double-down" aria-hidden="true"></i></p>
        				<div class="drop dropcategoria">
        					<?php
								// LISTA DE CATEGORIAS
								$arrayCategorias = array();
								$categorias=get_categories($args);
								foreach($categorias as $categoria):
								$arrayCategorias[$categoria->cat_ID] = $categoria->name;
								$nomeCategoria = $arrayCategorias[$categoria->cat_ID];
							?>
        					<a href="<?php echo get_category_link($categoria->cat_ID); ?>">
        						<h2><?php echo $nomeCategoria; ?></h2>
        					</a>
							<?php endforeach; ?>
        				</div>
        			</div>
            	</div>

            	<!-- ÁREA DE E-BOOK'S -->
            	<div class="col-md-3 col-sm-6">
            		
            		<div class="menu">
            			<p id="eboock"> E-BOOK'S GRATUITOS <i class="fa fa-angle-double-down" aria-hidden="true"></i></p>
            			<div class="drop eboock">
            				<a href="">
            					<h2>Call Center</h2>
            				</a>
            			</div>
            		</div>
            	</div>

            	<!-- ÁREA DE PESQUISA-->
            	<div class="col-md-6 col-sm-12">
            		<div class="areaPesquisa" id="pesquisa">
            	  		<input type="text" id="campoPesquisa"/>
							<input type="submit" value="&#xf002">
                    </div>

            	</div>

            </div>

		</div>
  	</div>

  	<!-- ÁREA DOS CONTEÚDOS -->
  	<section class="conteudos">		  		
  		<h6 class="hidden"><?php echo get_the_title() ?></h6>
  		<div class="container">
			<div class="row">

				<!-- ÁREA DE POSTS -->
				<div class="col-md-1">
					<div class="redesSociais">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url; ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
							<div class="icone1">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</div>
						</a>	
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $$current_url; ?>&title=<?php echo get_the_title(); ?>&summary=<?php $resumo = get_the_content();echo substr($resumo, 0, 150) . '...';  ?>&source=<?php echo get_site_url(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
							<div class="icone2">
								<i class="fa fa-linkedin" aria-hidden="true"></i>
							</div>
						</a>
					    <a href="https://plus.google.com/share?url=<?php echo $current_url; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
							<div class="icone3">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
						</a>
					</div>
				</div>

				<!-- ÁREA DE POSTS -->
				<div class="col-md-8">
					<div class="conteudoDoPost">

						<!-- MENU DE NAVEGAÇÃO -->
						<div class="menuPost">
							<nav>
							  <a href="<?php echo home_url('/'); ?>">Home
								<i class="fa fa-angle-double-right  " aria-hidden="true"></i>
							  </a>
							  <a href="<?php echo home_url('/'); ?>">Blog</a>
							    <i class="fa fa-angle-double-right  " aria-hidden="true"></i>
							  <span> <?php echo get_the_title() ?></span>								 
							</nav>
						</div>
						
						<!-- POST -->
						<div class="post">

							<div class="texto">
								<div class="tituloPost">
									<h4><?php echo get_the_title() ?></h4>
									<h5><?php echo $subtitulo_post ?></h5>
									<span>por <strong> <?php echo $author = get_the_author(); ?> </strong> em <?php the_time('j') ?>/<?php the_time('m') ?>/<?php the_time(' Y') ?></span>
								</div>
								<?php echo the_content() ?>
						   </div>

						   <div class="compartilhar">
							   	<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url; ?>&t=TITLE" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
									<div class="icone1">
								   		<p>Curtiu? <span> Compartilhe </span></p>										
							   			<div class="icone">
							   				<i class="fa fa-facebook" aria-hidden="true"></i>
							   			</div>						
								   	</div>
							   	</a>
					    	</div>

				    	</div>

						<!-- SESSÃO CARROSSEL DOS POST SIMILARES -->
	               		<section class="postSimilares">
	               			<?php 
								//LOOP DE POST CATEGORIA DESTAQUE				
								$postCategory = new WP_Query(array(
									'post_type'     => 'post',
									'posts_per_page'   => -1,
									'tax_query'     => array(
										array(
											'taxonomy' => 'category',
											'field'    => 'slug',
											'terms'    => 'similares',
											)
										)
									)
								);
							?>
							<!-- TÍTULO DA SESSÃO  -->
							<h6 class="hidden">Carrossel Post</h6>

							<div class="btnCarrossel">
								<div class="row">
									<div class="col-md-6">
										<p>Post's Similares</p>
									</div>
									<div class="col-md-6">
										<button id="btnCarrosselPostSimilaresLeft"><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
										<button id="btCcarrosselPostSimilaresRight"><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
									</div>
								</div>
							</div>

							<!-- CARROSSEL DE POSTS -->
							<div class="carrosselPostSimilares" id="carrosselPostSimilares">
								<?php 
					
								// LOOP DE POST
								while ( $postCategory->have_posts() ) : $postCategory->the_post();
									//FOTO DESTACADA
									$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$fotoPost = $fotoPost[0];
									
								?>		
								<!-- ITEM -->
								<div class="item">
									<!-- POST -->
									<div class="post">
										<a href="<?php echo get_permalink() ?>">
											<!-- IMAGEM DO POST -->
											<div class="imagem">
												<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
											</div>

											<!-- DESCRIÇÃO DO POST -->
											<div class="texto">
												<span><?php the_time('j') ?>/<?php the_time('m') ?>/<?php the_time(' Y') ?></span>
												<p><?php echo get_the_title() ?></p>
											</div>
										</a>
									</div>
								</div>
								<?php endwhile; wp_reset_query(); ?>
							</div>
						</section>	

			       </div>
			       
			       <div class="voltar">
				       	<a href="#topo">
				       		<div class="icone">
				       			<i class="fa fa-angle-double-up " aria-hidden="true"></i>
				       		</div>
				       		<p>Voltar ao Topo</p>
				       	</a>
			       </div>

				</div>		

				<!-- ÁREA DE SIDEBAR -->
				<div class="col-md-3">

					<aside>
						<div class="praticasSucesso">
							<img src="<?php bloginfo('template_directory'); ?>/img/imgLigacao.png" alt="">
							<h6> 9 Práticas de Sucesso </h6>
							<p>para um Call center mais eficaz</p>
							<a href=""> Saiba como</a>
						</div>

						<div class="assinarBlog">
							<p>Assine nosso <strong>Blog</strong></p>
							 <div class="enviar">
							    	
							    	<!--START Scripts : this is the script part you can add to the header of your theme-->
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-includes/js/jquery/jquery.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
									<script type="text/javascript">
                					
                					/* <![CDATA[ */
               						 var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_home_url() ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
              						  /* ]]> */
               						 </script><script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
									
									<!--END Scripts-->

									<div class="widget_wysija_cont html_wysija">
									<div id="msg-form-wysija-html5a6de662c646d-2" class="wysija-msg ajax"></div>
									<form id="form-wysija-html5a6de662c646d-2" method="post" action="#wysija" class="widget_wysija html_wysija">
										
    									<input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email" placeholder="Seu e-mail" value="" />
    
									    <span class="abs-req">
									        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
									    </span>
									    
										<div class="iconeSubmit">	
											<input class="wysija-submit-field" type="submit" value="Assinar!" />
    										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
    									</div>
    									<input type="hidden" name="form_id" value="2" />
    									<input type="hidden" name="action" value="save" />
    									<input type="hidden" name="controller" value="subscribers" />
    									<input type="hidden" value="1" name="wysija-page" />
        								<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 										</form>
									</div>

								</div>
						</div>						
					</aside>

					<!--AREA DOS POST -->
					<div class="conteudoVejaTambem">		  		

						<div class="vejaTambem">
							<a href="">
							    <h5> Veja Também										
									<i class="fa fa-angle-double-down" aria-hidden="true"></i>
								</h5>
							</a>
						</div>
						<?php 
							//LOOP DE POST DESTAQUES
							$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'rand', 'order' => 'desc', 'posts_per_page' => 4) );
							while ( $posts->have_posts() ) : $posts->the_post();

								$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoPost = $fotoPost[0];
						?>
						<!-- POST -->
						<div class="post">
							<a href="<?php echo get_permalink() ?> ">
								<div class="row">

								   <!-- IMAGEM DO POST -->
									<div class="col-md-4">
										<div class="imagem">
											<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
										</div>
									</div>

									<!-- DESCRIÇÃO DO POST -->
									<div class="col-md-8">
										<div class="texto">
											<span><?php the_time('j') ?>/<?php the_time('m') ?>/<?php the_time(' Y') ?></span>
											<p><?php echo get_the_title() ?></p>
										</div>
									</div>
								</div>
							</a>
						</div>
						<?php endwhile; wp_reset_query(); ?>
					</div>									
						
					<!--ÁREA DE TAGS -->				
					<div class="areaDeTags tags2">
						<?php $post_tags = get_the_tags(); if ( $post_tags ): ?>
						<h6>Tags </h6>
						<div class="tags">
							<?php foreach( $post_tags as $tag ): ?>
							<a href="<?php bloginfo('url');?>/tag/<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a>
							<?php endforeach; endif; ?>
					    </div>
					</div>		

				</div>

			</div>
		</div>
	</section>
	
	
</div>

<div class="pg">
	<div class="newsllater">
			<div class="container">
				<h6 class="hidden">Assinar o blog para receber notícias</h6>

				<div class="row">

					<!-- ÁREA DE NOTÍCIAS -->
					<div class="col-md-12">
				
						<div class="noticia">
							<h5>Não perca mais nenhum conteúdo!</h5>
							<p>Assine nosso <strong>Blog</strong> e receba novos posts em seu email</p>
							<div class="enviar">
								<!--START Scripts : this is the script part you can add to the header of your theme-->
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-includes/js/jquery/jquery.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.1"></script>
									<script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
									<script type="text/javascript">
                					
                					/* <![CDATA[ */
               						 var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_home_url() ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
              						  /* ]]> */
               						 </script><script type="text/javascript" src="<?php echo get_home_url() ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.1"></script>
									
									<!--END Scripts-->

									<div class="widget_wysija_cont html_wysija">
									<div id="msg-form-wysija-html5a6de662c646d-2" class="wysija-msg ajax"></div>
									<form id="form-wysija-html5a6de662c646d-2" method="post" action="#wysija" class="widget_wysija html_wysija">
								<input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email" placeholder="Coloque aqui seu e-mail" value="" />
    
							    <span class="abs-req">
							        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
							    </span>
							    
									<input class="wysija-submit-field" type="submit" value="Assinar!" />
									<input type="hidden" name="form_id" value="2" />
    									<input type="hidden" name="action" value="save" />
    									<input type="hidden" name="controller" value="subscribers" />
    									<input type="hidden" value="1" name="wysija-page" />
        								<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 										</form>
								</div>
							</div>
						</div>

					</div>
						
				</div>

			</div>
			
	</div>
</div>

<script>
	$('a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});
</script>
<?php
get_footer();
